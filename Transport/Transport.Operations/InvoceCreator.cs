﻿using System;
using System.Collections.Generic;
using Transport.Data;

namespace Transport.Operations
{
    public class InvoceCreator
    {
        public static void CreateIncomeInvoice(Order order, IEnumerable<InvoiceItemLine> cargoList, Store store, InvoiceRepository invoiceRepository)
        {
            
            var newInvoice = new Invoice(Invoice.InvoiceType.Incoming, Invoice.InvoiceStatus.Open, order, cargoList,
                store);
            
            invoiceRepository.Add(Guid.NewGuid(), newInvoice);
        }
        
        public static void CreateOutgoingInvoice(Order order, IEnumerable<InvoiceItemLine> cargoList, Store store, InvoiceRepository invoiceRepository)
        {
            var newInvoice = new Invoice(Invoice.InvoiceType.Outgoing, Invoice.InvoiceStatus.Open, order, cargoList,
                store);
            
            invoiceRepository.Add(Guid.NewGuid(), newInvoice);
        }
    }
}