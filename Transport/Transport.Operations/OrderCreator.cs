﻿using System;
using System.Collections.Generic;
using Transport.Data;

namespace Transport.Operations
{
    public class OrderCreator
    {
        public static void CreateNewOrder(Store departurePoint, Store destinationPoint,
            IEnumerable<OrderItemLine> cargoList, Transit transit, OrderRepository orderRepository)
        {
            
            var newOrder = new Order(departurePoint, destinationPoint, cargoList);
            
            newOrder.AddTransit(transit);
            
            orderRepository.Add(Guid.NewGuid(), newOrder);
            
        }
    }
}