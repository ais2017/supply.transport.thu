﻿using System;
using System.Collections.Generic;
using System.Linq;
using Transport.Data;

namespace Transport.Operations
{
    public class ScheduleViewer
    {
        
        
        public static List<Transit> GetPersonalSchedule(Employee employee, TransitRepository transitRepository)
        {
            var schedule = new List<Transit>();
            
            if (employee.Role == Employee.EmployeeRole.Driver)
                schedule = transitRepository.GetAllValues().Where(t => t.Driver == employee).ToList();
            else if (employee.Role == Employee.EmployeeRole.Forwarder)
                schedule = transitRepository.GetAllValues().Where(t => t.Forwarder == employee).ToList();
            else
                throw new ArgumentException("Employee role is not 'driver' or 'forwarder'");

            return schedule.Where(t=>t.ActualFinish > DateTime.Now).ToList();
        }

        public static List<Transit> GetPersonalScheduleInDataRange(Employee employee, DateTime starTime, DateTime endTime, TransitRepository transitRepository)
        {
            return GetPersonalSchedule(employee, transitRepository).Where(t => t.ActualStart >= starTime && t.ActualFinish <= endTime).ToList();
        }

        public static List<Transit> GetTransitHistory(TransitRepository transitRepository)
        {
            return transitRepository.GetAllValues().ToList();
        }

        
    }
}