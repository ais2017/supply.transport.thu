﻿namespace Transport
{
    public class InvoiceItemLine
    {
        public Cargo Cargo { get; set; }
        
        public int Quantity { get; set; }

        public InvoiceItemLine(Cargo cargo, int quantity)
        {
            this.Cargo = cargo;
            this.Quantity = quantity;
        }
    }
}