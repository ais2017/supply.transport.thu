﻿namespace Transport
{
    public class Employee
    {
        public string Name { get; private set; }

        public enum EmployeeRole
        {
            Driver,
            Forwarder,
            Manager
        }
        
        public EmployeeRole Role { get; private set; }

        public Employee(string name, EmployeeRole role)
        {
            this.Name = name;
            this.Role = role;
        }
    }
}