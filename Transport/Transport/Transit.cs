﻿using System;
using System.Collections.Generic;
using System.Data;

namespace Transport
{
    public class Transit
    {
        private DateTime PlanedStart { get; set; }
        
        private DateTime PlanedFinish { get; set; }
        
        public DateTime ActualStart { get; set; }
        
        public DateTime ActualFinish { get; set; }
        
        public Employee Driver { get; private set; }
        
        public Employee Forwarder { get; private set; }
        
        public IEnumerable<InvoicePair> InvoiceList { get; private set; }

        public Transit(DateTime pStart, DateTime pFinish, Employee driver, Employee forwarder,
            IEnumerable<InvoicePair> invoiceList)
        {
            this.PlanedStart = pStart;
            this.PlanedFinish = pFinish;
            this.ActualStart = pStart;
            this.ActualFinish = pFinish;
            if (driver.Role != Employee.EmployeeRole.Driver)
                throw new ArgumentException("Employee role is not 'Driver' in appropriate argument");
            else
                this.Driver = driver;
            if (forwarder.Role != Employee.EmployeeRole.Forwarder)
                throw new ArgumentException("Employee role is not 'Forwarder' in appropriate argument");
            else
                this.Forwarder = forwarder;

            this.InvoiceList = invoiceList;

        }

        public void ShipOff()
        {
            
            this.ActualStart = DateTime.Now;
        }

        public void Receive()
        {
            this.ActualFinish = DateTime.Now;
        }
    }
}