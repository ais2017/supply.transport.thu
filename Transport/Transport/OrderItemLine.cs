﻿namespace Transport
{
    public class OrderItemLine
    {
        public Cargo Cargo { get; set; }
        
        public int Quantity { get; set; }

        public OrderItemLine(Cargo cargo, int quantity)
        {
            this.Cargo = cargo;
            this.Quantity = quantity;
        }
    }
}