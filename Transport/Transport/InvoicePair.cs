﻿namespace Transport
{
    public class InvoicePair
    {
        public Invoice IncomeInvoice { get; private set; }
        
        public Invoice OutgoingInvoice { get; private set; }

        public InvoicePair(Invoice income, Invoice outgoing)
        {
            this.IncomeInvoice = income;
            this.OutgoingInvoice = outgoing;
        }
    }
}