﻿using System.Collections.Generic;
using System.Linq;

namespace Transport
{
    public class Invoice
    {
        public enum InvoiceType
        {
            Incoming,
            Outgoing
        }

        public enum InvoiceStatus
        {
            Open,
            InProgres,
            Closed
        }
        
        public InvoiceType Type { get; private set; }
        
        public InvoiceStatus Status { get; set; }
        
        public int SummaryWeight { get; set; }
        
        public Order Order { get; set; }
        
        public IEnumerable<InvoiceItemLine> CargoList { get; set; }
        
        public Store Store { get; set; }

        public Invoice(InvoiceType type, InvoiceStatus status, Order order, IEnumerable<InvoiceItemLine> cargoList,
            Store store)
        {
            this.CargoList = cargoList;
            this.Order = order;
            this.Status = status;
            this.Store = store;
            this.Type = type;
            this.SummaryWeight = this.CargoList.Sum(cargo => (cargo.Cargo.Weight * cargo.Quantity));
        }
    }
}