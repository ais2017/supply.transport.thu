﻿using System.Net.Sockets;

namespace Transport
{
    public class Store
    {
        public string Address { get; private set; }

        public enum StoreType
        {
            Internal,
            External
        }
        
        public StoreType Type { get; private set; }

        public Store(string address, StoreType type)
        {
            this.Address = address;
            this.Type = type;
        }
    }
}