﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

namespace Transport
{
    public class Order
    {
        public IEnumerable<Transit> TransitList { get; private set; }
        
        public Store DeparturePoint { get; set; }
        
        public Store DestinationPoint { get; set; }

        public Guid Id;

        public IEnumerable<OrderItemLine> CargoList { get; set; }

        public Order(Store departurePoint, Store destinationPoint, IEnumerable<OrderItemLine> cargoList)
        {
            if (departurePoint == destinationPoint)
                throw new ArgumentException("departure point cannot be same as destination!");
            this.DeparturePoint = departurePoint;
            this.DestinationPoint = destinationPoint;
            this.CargoList = cargoList;
            this.Id = new Guid();
        }

        public void AddTransit(Transit transit)
        {
            var newList = new List<Transit>();
            newList.Add(transit);
            this.TransitList = TransitList.Concat(newList);
        }
        
    }
}