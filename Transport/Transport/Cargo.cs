﻿namespace Transport
{
    public class Cargo
    {
        public string VendorCode { get; private set; }

        public int Weight { get; private set; }

        public Cargo(string vendorCode, int weight)
        {
            this.VendorCode = vendorCode;
            this.Weight = weight;
        }
    }
}