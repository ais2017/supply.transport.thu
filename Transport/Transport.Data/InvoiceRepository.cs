﻿using System;
using System.Collections.Generic;

namespace Transport.Data
{
    public abstract class InvoiceRepository : IRepository<Guid, Invoice>
    {
        private Dictionary<Guid, Invoice> Invoices { get; set; }
        public IEnumerable<Guid> Keys { get; private set; }
        public IEnumerable<Invoice> Values { get; private set; }
        public IEnumerable<Guid> GetAllKeys()
        {
            return Invoices.Keys;
        }

        public IEnumerable<Invoice> GetAllValues()
        {
            return Invoices.Values;
        }

        public abstract void GetAll();

        public abstract void Add(Guid key, Invoice value);

        public abstract void Remove(Guid key);
    }
}