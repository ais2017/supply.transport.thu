﻿using System;
using System.Collections.Generic;

namespace Transport.Data
{
    public abstract class OrderRepository : IRepository<Guid, Order>
    {
        private Dictionary<Guid, Order> Orders { get; set; }
        public IEnumerable<Guid> Keys { get; private set; }
        public IEnumerable<Order> Values { get; private set; }
        public IEnumerable<Guid> GetAllKeys()
        {
            return Orders.Keys;
        }

        public IEnumerable<Order> GetAllValues()
        {
            return Orders.Values;
        }

        public abstract void GetAll();

        public abstract void Add(Guid key, Order value);

        public abstract void Remove(Guid key);
    }
}