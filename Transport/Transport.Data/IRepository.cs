﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace Transport.Data
{
    public interface IRepository<TKey, TValue>
    {
        
        IEnumerable<TKey> Keys { get; }
        
        IEnumerable<TValue> Values { get; }

        IEnumerable<TKey> GetAllKeys();

        IEnumerable<TValue> GetAllValues();

        void GetAll();

        void Add(TKey key, TValue value);

        void Remove(TKey key);

    }
}