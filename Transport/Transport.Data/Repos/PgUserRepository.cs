﻿using System;
using System.Collections.Generic;
using System.Data;
using Npgsql;

namespace Transport.Data.Repos
{
    public class PgUserRepository : UserRepository
    {
        public PgUserRepository()
        {
            this.Employees = new Dictionary<Guid, Employee>();
        }
        
        public override void GetAll()
        {
            var connectionString = $"Server=localhost;Port=5432;User Id=postgres;Password=1;Database=Transport_db;";
            
            NpgsqlConnection connection = new NpgsqlConnection(connectionString);
            connection.Open();
            
            string sql = "SELECT * FROM Employee";
            
            NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(sql, connection);
            var dataSet = new DataSet();
            var dataTable = new DataTable();
            
            dataSet.Reset();
            
            adapter.Fill(dataSet);
            dataTable = dataSet.Tables[0];
            
            connection.Close();

            foreach(DataTable table in dataSet.Tables)
            {
                foreach(DataRow row in table.Rows)
                {
                    
                    var role = Employee.EmployeeRole.Manager;
                    switch (row[2])
                    {
                        case "Driver":
                        {
                            role = Employee.EmployeeRole.Driver;
                            break;
                        }
                        case "Forwarder":
                        {
                            role = Employee.EmployeeRole.Forwarder;
                            break;
                        }
                    }
                    
                    var employee = new Employee(row[1].ToString(), role);
                    
                    //this.Employees.Add(Guid.Parse(row[0].ToString()), employee);
                    
                    var guid = new Guid();
                    this.Employees.Add(guid, employee);
                    Console.WriteLine(this.Employees[guid].Name + " " + this.Employees[guid].Role);

//                    foreach (DataColumn column in table.Columns)
//                    {
//                        Console.WriteLine(row[column]);
//                    }
                }
            }
        }

        public override void Add(Guid key, Employee value)
        {
            throw new NotImplementedException();
        }

        public override void Remove(Guid key)
        {
            throw new NotImplementedException();
        }
    }
}