﻿using System;
using System.Collections.Generic;

namespace Transport.Data
{
    public abstract class UserRepository : IRepository<Guid, Employee>
    {
        protected Dictionary<Guid, Employee> Employees { get; set; }
        public IEnumerable<Guid> Keys { get; private set; }
        public IEnumerable<Employee> Values { get; private set; }
        public IEnumerable<Guid> GetAllKeys()
        {
            return Employees.Keys;
        }

        public IEnumerable<Employee> GetAllValues()
        {
            return Employees.Values;
        }

        public abstract void GetAll();

        public abstract void Add(Guid key, Employee value);

        public abstract void Remove(Guid key);
    }
}