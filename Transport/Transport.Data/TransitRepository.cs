﻿using System;
using System.Collections.Generic;

namespace Transport.Data
{
    public abstract class TransitRepository : IRepository<Guid, Transit>
    {
        protected Dictionary<Guid, Transit> Transits { get; set; }
        public IEnumerable<Guid> Keys { get; private set; }
        public IEnumerable<Transit> Values { get; private set; }
        public IEnumerable<Guid> GetAllKeys()
        {
            return Transits.Keys;
        }

        public IEnumerable<Transit> GetAllValues()
        {
            return Transits.Values;
        }

        public abstract void GetAll();

        public abstract void Add(Guid key, Transit value);

        public abstract void Remove(Guid key);
    }
}