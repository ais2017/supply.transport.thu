﻿using System;
using System.Collections.Generic;
using Transport.Data;

namespace Transport.Tests.Doubles
{
    public class TransitRepositoryMoq : TransitRepository
    {
        public override void GetAll()
        {
            this.Transits = new Dictionary<Guid, Transit>();
            var driver = new Employee("aaa", Employee.EmployeeRole.Driver);
            
            var forw = new Employee("bbb", Employee.EmployeeRole.Forwarder);
            var store1 = new Store("ccc", Store.StoreType.External);
            var store2 = new Store("ddd", Store.StoreType.Internal);
            var cargo = new Cargo("eee", 11);
            var itemline = new OrderItemLine(cargo, 2);
            var itemlinelist = new List<OrderItemLine>();
            itemlinelist.Add(itemline);
            var invitemline = new InvoiceItemLine(cargo, 2);
            var invitemlinelist = new List<InvoiceItemLine>();
            invitemlinelist.Add(invitemline);
            var ininv = new Invoice(Invoice.InvoiceType.Incoming, Invoice.InvoiceStatus.Open,
                new Order(store1, store2, itemlinelist), invitemlinelist, store1);
            var outinv = new Invoice(Invoice.InvoiceType.Outgoing, Invoice.InvoiceStatus.Open,
                new Order(store1, store2, itemlinelist), invitemlinelist, store2);
            var invpair = new InvoicePair(ininv, outinv);
            var invpairlist = new List<InvoicePair>();
            invpairlist.Add(invpair);
            
            var tr = new Transit(DateTime.Now, DateTime.Parse("2017.12.01"), driver, forw, invpairlist);
            tr.ActualFinish = DateTime.Parse("2017.12.01");
            tr.ActualStart = DateTime.Now;

            this.Transits.Add(new Guid(), tr);
        }

        public override void Add(Guid key, Transit value)
        {
            throw new NotImplementedException();
        }

        public override void Remove(Guid key)
        {
            throw new NotImplementedException();
        }
    }
}