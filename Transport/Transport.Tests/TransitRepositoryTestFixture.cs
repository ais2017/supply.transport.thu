﻿using NUnit.Framework;
using Transport.Operations;
using Transport.Tests.Doubles;

namespace Transport.Tests
{
    [TestFixture]
    public class TransitRepositoryTestFixture
    {
        [Test]
        public void GetAllTest()
        {
            var transitRepositoryMoq = new TransitRepositoryMoq();
            transitRepositoryMoq.GetAll();
            Assert.IsNotEmpty(transitRepositoryMoq.GetAllValues());
        }
        
    }
}