﻿using System;
using NUnit.Framework;
using Transport.Operations;
using Transport.Tests.Doubles;

namespace Transport.Tests
{
    [TestFixture]
    public class ScheduleTestFixture
    {
        [Test]
        public void ScheduleViwerTest()
        {
            var transitRepositoryMoq = new TransitRepositoryMoq();
            transitRepositoryMoq.GetAll();
            Assert.IsNotEmpty(ScheduleViewer.GetTransitHistory(transitRepositoryMoq));
        }

        [Test]
        public void ScheduleViewerInRangeTest()
        {
            var transitRepositoryMoq = new TransitRepositoryMoq();
            transitRepositoryMoq.GetAll();
            var start = DateTime.MinValue;
            var end = DateTime.Parse("2017.12.02");
            var empl = new Employee("fff", Employee.EmployeeRole.Driver);
            Assert.IsEmpty(ScheduleViewer.GetPersonalScheduleInDataRange(empl,start,end,transitRepositoryMoq));
        }
    }
}