﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using System.Linq;

namespace Transport.Tests
{
    [TestFixture]
    public class OrderingTestFixture
    {
        [Test]
        public void OrderSameStorePointsTest()
        {
            var cargo = new Cargo("12345", 20);
            var cargoItem = new OrderItemLine(cargo, 20);
            var cargoList = new List<OrderItemLine>();
            cargoList.Add(cargoItem);
            var store = new Store("address", Store.StoreType.Internal);
            //var order = new Order(store, store, cargoList);
            Assert.Throws(typeof(ArgumentException),() => new Order(store, store, cargoList));
        }
    }
}