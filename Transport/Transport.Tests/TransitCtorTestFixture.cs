﻿using System;
using System.Collections.Generic;
using NUnit.Framework;

namespace Transport.Tests
{
    [TestFixture]
    public class TransitCtorTestFixture
    {
        [Test]
        public void TransitCtorTest()
        {
            var driver = new Employee("aaa", Employee.EmployeeRole.Driver);
            var forw = new Employee("bbb", Employee.EmployeeRole.Forwarder);
            
            var store1 = new Store("ccc", Store.StoreType.External);
            var store2 = new Store("ddd", Store.StoreType.Internal);
            
            var cargo = new Cargo("12345", 20);
            var cargoItem = new OrderItemLine(cargo, 20);
            var cargoList = new List<OrderItemLine>();
            cargoList.Add(cargoItem);
            
            var cargoinv = new Cargo("12345", 20);
            var cargoIteminv = new InvoiceItemLine(cargoinv, 20);
            var cargoListinv = new List<InvoiceItemLine>();
            cargoListinv.Add(cargoIteminv);
            
            
            var order = new Order(store1, store2, cargoList);

            var ininv = new Invoice(Invoice.InvoiceType.Incoming, Invoice.InvoiceStatus.Open, order, cargoListinv,
                store1);
            var outinv = new Invoice(Invoice.InvoiceType.Incoming, Invoice.InvoiceStatus.Open, order, cargoListinv,
                store2);
            
            var invpair = new InvoicePair(ininv, outinv);
            
            var invpairlist = new List<InvoicePair>();
            invpairlist.Add(invpair);

            Assert.DoesNotThrow(
                () => new Transit(DateTime.Now, DateTime.MaxValue, driver, forw, invpairlist));

            Assert.Throws(typeof(ArgumentException),
                () => new Transit(DateTime.Now, DateTime.MaxValue, forw, driver, invpairlist));


        }
        
    }
}